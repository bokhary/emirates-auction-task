# Emirates Auction Task

* ### To solve the Emirates Auction Task, I used MVVM design pattern , 
* ### and that  because MVVM seperate the responsibility of business from ViewController to make more readable and testable, and prevent it from massive code.
* ### also  ViewModel hold all work of fetching and parsing data from network.
* ### There is another design pattern we can use , the MVC pattern but that has some issues where our ViewController is responsible of all logic of View and model and that make our controller has much code.
