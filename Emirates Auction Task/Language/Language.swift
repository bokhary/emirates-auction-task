//
//  Language.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/10/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

enum LocalizedWord: String {
    case warning = "warning"
    case restartAppMessage = "restartAppMessage"
    case pullToRefresh = "pullToRefresh"
    case sortBy = "sortBy"
    case endDate = "endDate"
    case price = "price"
    case year = "year"
    case ok = "ok"
}

class Language {
    
    private init() {}
    
    class func currentLanguage() -> String {
        let langArray = UserDefaults.standard.object(forKey: "AppleLanguages") as! NSArray
        let current = (langArray.firstObject as! String).components(separatedBy: "-")
        
        return current.first!
    }
    
    class func isArabic() -> Bool {
        return currentLanguage() == "ar"
    }
    
    class func setLanguage() {
        if isArabic() {
            UserDefaults.standard.set(["en-EG"], forKey: "AppleLanguages")
        }
        else {
            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
        }
        UserDefaults.standard.synchronize()
    }
    
    class func localizeWord(word: LocalizedWord) -> String {
        return NSLocalizedString(word.rawValue, comment: word.rawValue)
    }
}
