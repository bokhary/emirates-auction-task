//
//  CarTableViewCell.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit
import Kingfisher

class CarTableViewCell: UITableViewCell {
    
    //MARK: Variables
    static let identifier = "CarCell"
    private var car: Car!
    private var currentEndDate = 0
    private var timer: Timer!
    
    //MARK: Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carPriceLabel: UILabel!
    @IBOutlet weak var lotLabel: UILabel!
    @IBOutlet weak var bidsLabel: UILabel!
    @IBOutlet weak var timeLeftLabel: UILabel!
    
    //MARK: Actions
    
    //MARK: Methods
    override func awakeFromNib() {
        Helper.round(view: containerView, cornerRadius: 5)
    }
    override func prepareForReuse() {
        timer.invalidate()
    }
    func configure(car: Car) {
        self.car = car
        setCarImage()
        setCarInfo()
        setTimeLabel()
    }
    
    private func setCarImage() {
        
        let width = carImageView.frame.width * UIScreen.main.scale
        let height = carImageView.frame.height * UIScreen.main.scale
        
        let imageURL = URL.init(string: WebServices.pathOf(imagePath: car.image, with: width, and: height))
        carImageView.kf.setImage(with: imageURL)
    }
    
    private func setCarInfo() {
        favouriteButton.isSelected = false
        
        carNameLabel.text = "\(Language.isArabic() ? car.makeAr : car.makeEn) \(Language.isArabic() ? car.modelAr : car.modelEn) \(car.year)"
        carPriceLabel.attributedText = buildPriceText()
        lotLabel.text = "\(car.auctionInfo.lot)"
        bidsLabel.text = "\(car.auctionInfo.bids)"
        if currentEndDate == 0 {
            currentEndDate = car.auctionInfo.endDate
        }
    }
    
    private func buildPriceText() -> NSMutableAttributedString {
        let font = UIFont.systemFont(ofSize: 17, weight: .medium)
        let fontSuperscript = UIFont.systemFont(ofSize: 15)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let priceWithCommaString = numberFormatter.string(from: NSNumber(value: car.auctionInfo.currentPrice))!
        
        let priceAttributed = NSMutableAttributedString(string: priceWithCommaString, attributes: [.font: font])
        
        let currency = Language.isArabic() ? car.auctionInfo.currencyAr : car.auctionInfo.currencyEn
        let superscriptAttributed = NSAttributedString(string: currency, attributes: [.font: fontSuperscript, .baselineOffset: 10])
        
        priceAttributed.append(superscriptAttributed)
        return priceAttributed
    }
    
    private func setTimeLabel() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            if self.currentEndDate <= 0 {
                timer.invalidate()
            } else {
                self.updateTimeLeftLabel()
            }
        }
    }
    
    private func updateTimeLeftLabel() {
        let updatedTime = getTimeLeftTuple(endDate: self.currentEndDate)
        
        currentEndDate = currentEndDate - 1
        timeLeftLabel.text = updatedTime.0
        if updatedTime.1 == 0 && updatedTime.2 < 5 {
            timeLeftLabel.textColor = .red
        } else {
            timeLeftLabel.textColor = .darkGray
        }
    }
    private func getTimeLeftTuple(endDate: Int) -> (String,Int,Int) {
        let hours = endDate / 3600
        let minutes = endDate / 60 % 60
        let seconds = endDate % 60
        
        let timeString = ((hours < 10) ? "0" : "") + String(hours) + ":" + ((minutes < 10) ? "0" : "") + String(minutes) + ":" + ((seconds < 10) ? "0" : "") + String(seconds)
        
        return (timeString,hours,seconds)
    }
}

