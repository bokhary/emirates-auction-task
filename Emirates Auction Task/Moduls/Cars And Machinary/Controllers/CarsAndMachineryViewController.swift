//
//  CarsAndMachineryViewController.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit

class CarsAndMachineryViewController: UIViewController {
    
    //MARK: Variables
    private var firstTime = true
    
    //MARK: View lifcycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup views
        setupViews()
        
        // load cars
        loadCars()
    }
    
    
    //MARK: Outlets
    @IBOutlet var viewModel: CarsAndMachineryViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var actionsContainerView: UIView!
    
    //MARK: Actions
    @IBAction func sortCarsButtonAction(_ sender: UIButton) {
        showSortingAlert()
    }
    
    
    //MARK: Methods
    func setupViews() {
        
        // setup language bar button item
        setupLanguageBarButton()
        
        // round and set shadow to actions container view
        Helper.round(view: actionsContainerView, cornerRadius: 4)
        
        // bind view with its view model by delegate pattern 
        viewModel.delegate = self
        
        // setup referesh control
        setupRefereshControl()
    }
    
    func setupLanguageBarButton() {
        let currentLanguage = Language.isArabic() ? "En"  : "ع"
        let languageBarButton = UIBarButtonItem.init(title: currentLanguage, style: .done, target: self, action: #selector(changeLanguage))
        languageBarButton.tintColor = .white
        navigationItem.rightBarButtonItem = languageBarButton
    }
    
    @objc func changeLanguage() {
        Language.setLanguage()
        Helper.alert(title: Language.localizeWord(word: .warning), message: Language.localizeWord(word: .restartAppMessage), viewController: self)
    }
    func loadCars() {
        viewModel.fetchCars()
    }
    
    func setupRefereshControl() {
        // add action for refresh controle of tablView
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.attributedTitle = NSAttributedString.init(string: Language.localizeWord(word: .pullToRefresh))
        tableView.refreshControl?.addTarget(self, action: #selector(refrechCars), for: .valueChanged)
    }
    
    @objc func refrechCars() {
        loadCars()
    }
    
    func showSortingAlert() {
        let alert = UIAlertController.init(title: Language.localizeWord(word: .sortBy), message: "", preferredStyle: .alert)
        
        let sortByEndDateAction = UIAlertAction.init(title: Language.localizeWord(word: .endDate), style: .default) { _ in
            self.viewModel.sortBy(type: .endDate)
            self.tableView.reloadData()
        }
        
        let sortByPriceAction = UIAlertAction.init(title: Language.localizeWord(word: .price), style: .default) { _ in
            self.viewModel.sortBy(type: .price)
            self.tableView.reloadData()
        }
        
        let sortByYearAction = UIAlertAction.init(title: Language.localizeWord(word: .year), style: .default) { _ in
            self.viewModel.sortBy(type: .year)
            self.tableView.reloadData()
        }
        
        alert.addAction(sortByEndDateAction)
        alert.addAction(sortByPriceAction)
        alert.addAction(sortByYearAction)
        
        present(alert, animated: true, completion: nil)
    }
}

extension CarsAndMachineryViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CarTableViewCell.identifier, for: indexPath) as! CarTableViewCell
        
        cell.configure(car: viewModel.carOf(index: indexPath.row))
        return cell
    }
}

extension CarsAndMachineryViewController: ViewModelDelegate {
    
    func willLoadData() {
        activityIndicator.startAnimating()
    }
    
    func didLoadData() {
        tableView.reloadData()
        if viewModel.getRefershInterVal() != 0 && firstTime {
            firstTime = false
            Timer.scheduledTimer(withTimeInterval: TimeInterval(self.viewModel.getRefershInterVal()), repeats: true) { (timer) in
                self.loadCars()
            }
        }
    }
    
    func didLoadWithFail(message: String) {
        Helper.alert(title: Language.localizeWord(word: .warning), message: message, viewController: self)
    }
    
    func stopLoading() {
        activityIndicator.stopAnimating()
        if tableView.refreshControl!.isRefreshing {
            tableView.refreshControl?.endRefreshing()
        }
    }
    
    
}

