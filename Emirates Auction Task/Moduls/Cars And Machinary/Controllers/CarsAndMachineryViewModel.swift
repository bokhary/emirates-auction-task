//
//  CarsAndMachineryViewModel.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit

enum SortType {
    case endDate
    case price
    case year
    case none
}

class CarsAndMachineryViewModel: NSObject {
    
    //MARK: Variables
    private var carRoot: CarRoot!
    private var sortType: SortType = .none
    weak var delegate: ViewModelDelegate!
    
    //MARK: Methods
    
    func fetchCars() {
        
        delegate.willLoadData()
        
        NetworkManager.request(serviceName: WebServices.carssService(), method: .get) { (result) in
            
            self.delegate.stopLoading()
            
            switch result {
            case .success(let data):
                self.parseData(data: data)
                self.sortBy(type: self.sortType)
                self.delegate.didLoadData()
            case .fail(let failMessage):
                self.delegate.didLoadWithFail(message: failMessage)
            }
        }
    }
    
    private func parseData(data: Data) {
        do {
            let decoder = JSONDecoder()
            carRoot = try decoder.decode(CarRoot.self, from: data)
        }
        catch {
            print("Errot \(error.localizedDescription)")
        }
    }
    
    func sortBy(type: SortType) {
        self.sortType = type
        
        switch type {
        case .endDate:
            sortByEndDate()
        case .price:
            sortByPrice()
        case .year:
            sortByYear()
        case .none:
            break
        }
    }
    
    private func sortByEndDate() {
        carRoot.cars.sort { (car1, car2) -> Bool in
            car1.auctionInfo.endDate > car2.auctionInfo.endDate
        }
    }
    private func sortByPrice() {
        carRoot.cars.sort { (car1, car2) -> Bool in
            car1.auctionInfo.currentPrice > car2.auctionInfo.currentPrice
        }
    }
    private func sortByYear() {
        carRoot.cars.sort { (car1, car2) -> Bool in
            car1.year > car2.year
        }
    }
    
    func getRefershInterVal() -> Int {
        guard let carRoot = carRoot else {
            return 0
        }
        return carRoot.refreshInterval
    }
    
    func numberOfSections() -> Int {
        guard let _ = carRoot else {
            return 0
        }
        return 1
    }
    
    func numberOfRows() -> Int {
        guard let carRoot = carRoot else {
            return 0
        }
        return carRoot.cars.count
    }
    
    func carOf(index: Int) -> Car {
        return carRoot.cars[index]
    }
}
