//
//  NetworkResponse.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

/**
 Result enum to handle response of network in its
 success and fails state
 */
enum Result {
    case success(Data)
    case fail(String)
}
