//
//  WebServices.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import UIKit

/**
 WebServices is class that contain all our services that
 we need to request
 */
class WebServices {
    private init() {}
    
    class func carssService() -> String {
        return Constants.baseURL + "carsonline"
    }
    
    class func pathOf(imagePath: String, with width: CGFloat, and height: CGFloat) -> String {
        return imagePath.replacingOccurrences(of: "[w]", with: "\(width)").replacingOccurrences(of: "[h]", with: "\(height)")
    }
}
