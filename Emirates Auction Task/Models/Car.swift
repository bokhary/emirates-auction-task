//
//  Car.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

struct CarRoot: Decodable {
    let alertEn: String
    let alertAr: String
    let refreshInterval: Int
    let ticks: String
    let count: Int
    let endDate: Int
    let sortOption: String
    let sortDirection: String
    var cars: [Car]
    
    private enum CodingKeys: String, CodingKey {
        case alertEn, alertAr, count, endDate, sortOption, sortDirection
        case refreshInterval = "RefreshInterval"
        case ticks = "Ticks"
        case cars = "Cars"
    }
}

struct Car: Decodable {
    let carID: Int
    let image: String
    let descriptionAr: String
    let descriptionEn: String
    let imgCount: Int
    let sharingLink: String
    let sharingMsgEn: String
    let sharingMsgAr: String
    let mileage: String
    let makeID: Int
    let modelID: Int
    let bodyId: Int
    let year: Int
    let makeEn: String
    let makeAr: String
    let modelEn: String
    let modelAr: String
    let bodyEn: String
    let bodyAr: String
    let auctionInfo: AuctionInfo
    
    private enum CodingKeys: String, CodingKey {
        case carID, image, descriptionAr, descriptionEn, imgCount, sharingLink, sharingMsgEn, sharingMsgAr, mileage, makeID, modelID, bodyId, year, makeEn, makeAr, modelEn, modelAr, bodyEn, bodyAr
        case auctionInfo = "AuctionInfo"
    }
}

struct AuctionInfo: Decodable {
    let bids: Int
    let endDate: Int
    let endDateEn: String
    let endDateAr: String
    let currencyEn: String
    let currencyAr: String
    let currentPrice: Int
    let minIncrement: Int
    let lot: Int
    let priority: Int
    let vatPercent: Int
    let isModified: Int
    let itemid: Int
    let iCarId: Int
    let iVinNumber: String
    
    private enum CodingKeys: String, CodingKey {
        case bids, endDate, endDateEn, endDateAr, currencyEn, currencyAr, currentPrice, minIncrement, lot, priority, isModified, itemid, iCarId, iVinNumber
        case vatPercent = "VATPercent"
    }
    
}
