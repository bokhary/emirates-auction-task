//
//  Constants.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

enum Constants {
    static let baseURL = "http://api.emiratesauction.com/v2/"
}
