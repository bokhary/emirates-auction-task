//
//  ViewModelDelegate.swift
//  Emirates Auction Task
//
//  Created by Elsayed Hussein on 7/9/18.
//  Copyright © 2018 Elsayed Hussein. All rights reserved.
//

import Foundation

protocol ViewModelDelegate: class {
    func willLoadData()
    func didLoadData()
    func didLoadWithFail(message: String)
    func stopLoading()
}
